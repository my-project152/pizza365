
var gSelectedMenuStructure = {
    menuName: "...",    // S, M, L
    duongKinhCM: 0,
    suongNuong: 0,
    saladGr: 0,
    drink: 0,
    priceVND: 0
};

var gKhachHang = {
    fullName: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    loiNhan: "",
    maGiamGia: "",
};

var gDrink = null;

var gPercent = 0;

var gSelectedPizzaType = null;

$(document).ready(function () {
    getDrink();
})

$(document).on("click", "#sizeS", function () {
    onBtnSizeSCLick();
});

$(document).on("click", "#sizeM", function () {
    onBtnSizeMCLick();
});

$(document).on("click", "#sizeL", function () {
    onBtnSizeLCLick();
});

$(document).on("click", "#hawaii", function () {
    onBtnHawaiiClick();
});

$(document).on("click", "#haisan", function () {
    onBtnHaiSanClick();
});

$(document).on("click", "#thithunkhoi", function () {
    onBtnThitHunKhoiClick();
});

$(document).on("click", "#btn-check", function () {
    getUserData(gKhachHang);

    var vValidate = validateData(gKhachHang);

    if (vValidate) {

        showClientInfo();
    }

});

$(document).on("click", "#btn-create-order", function () {
    onBtnCreateOrderClick();
});

function onBtnCreateOrderClick() {
    $("#show-order-modal").modal("hide");

    gDrink = $("#select-drink option:selected").text();
    var vPrice = gSelectedMenuStructure.priceVND - (gSelectedMenuStructure.priceVND * parseInt(gPercent)) / 100;
    var vObjectRequest = {
        kichCo: gSelectedMenuStructure.menuName,
        duongKinh: gSelectedMenuStructure.duongKinhCM,
        suon: gSelectedMenuStructure.suongNuong,
        salad: gSelectedMenuStructure.saladGr,
        loaiPizza: gSelectedPizzaType,
        idVourcher: gKhachHang.maGiamGia,
        idLoaiNuocUong: gDrink,
        soLuongNuoc: gSelectedMenuStructure.drink,
        hoTen: gKhachHang.fullName,
        thanhTien: vPrice,
        email: gKhachHang.email,
        soDienThoai: gKhachHang.soDienThoai,
        diaChi: gKhachHang.diaChi,
        loiNhan: gKhachHang.loiNhan
    };

    console.log(vObjectRequest);


    $.ajax({
        url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
        type: "POST",
        async: false,
        contentType: "application/json",
        data: JSON.stringify(vObjectRequest),
        success: function (order) {
            $("#inp-result").val(order.orderId);
        },
        error: function () {
            console.log("fail");
        }
    });

    $("#show-orderid-modal").modal("show");
}

function showClientInfo() {
    var vDrinkName = $("#select-drink option:selected").text();
    if (gKhachHang.maGiamGia != "") {
        checkVoucherId();
    }

    var vPrice = gSelectedMenuStructure.priceVND - (gSelectedMenuStructure.priceVND * parseInt(gPercent)) / 100;

    $("#input-name").val(gKhachHang.fullName);
    $("#input-phonenumber").val(gKhachHang.soDienThoai);
    $("#input-email").val(gKhachHang.email);
    $("#input-address").val(gKhachHang.diaChi);
    $("#input-message").val(gKhachHang.loiNhan);
    $("#input-voucherid").val(gKhachHang.maGiamGia);
    $("#input-info").val(`Xác nhận: ` + gKhachHang.fullName + `, ` + gKhachHang.soDienThoai + `, ` + gKhachHang.diaChi
        + `\nMenu: ` + gSelectedMenuStructure.menuName + `, sườn nướng: ` + gSelectedMenuStructure.suongNuong + `, loại nước: ` + vDrinkName + `, nước: ` + gSelectedMenuStructure.drink
        + `\nLoại Pizza: ` + gSelectedPizzaType + `, Giá: ` + gSelectedMenuStructure.priceVND + `, Mã giảm giá: ` + gKhachHang.maGiamGia
        + `\nPhải thanh toán: ` + vPrice + ` vnd (giảm giá ` + gPercent + `%)`);

    $("#show-order-modal").modal("show");

}

function checkVoucherId() {
    $.ajax({
        url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail" + "/" + gKhachHang.maGiamGia,
        type: "GET",
        dataType: "json",
        async: false,
        success: function (voucherObj) {
            gPercent = voucherObj.phanTramGiamGia;
            console.log(gPercent);
        },
        error: function () {
            alert("mã giảm giá không tồn tại");
        }
    })
}

function getUserData(paramUser) {
    paramUser.fullName = $("#inp-fullname").val().trim();
    paramUser.email = $("#inp-email").val().trim();
    paramUser.soDienThoai = $("#inp-dien-thoai").val().trim();
    paramUser.diaChi = $("#inp-dia-chi").val().trim();
    paramUser.loiNhan = $("#inp-message").val().trim();
    paramUser.maGiamGia = $("#inp-voucherid").val().trim();
}

function validateData(paramUser) {
    if (gSelectedMenuStructure.menuName == "...") {
        alert("Vui lòng chọn combo");
        return false;
    }
    if (gSelectedPizzaType == null) {
        alert("Vui lòng chọn loại");
        return false;
    }
    if (paramUser.fullName == "") {
        alert("Vui lòng nhập tên");
        return false;
    }
    if (paramUser.email == "") {
        alert("Vui lòng nhập Email");
        return false;
    }
    if (paramUser.email.includes("@") == false) {
        alert("Email không hợp lệ");
        return false;
    }
    if (paramUser.soDienThoai == "") {
        alert("Vui lòng nhập số điện thoại");
        return false;
    }
    if (isNaN(paramUser.soDienThoai) == true) {
        alert("Số điện thoại không hợp lệ");
        return false;
    }

    if (paramUser.diaChi == "") {
        alert("Vui lòng địa chỉ");
        return false;
    }
    return true;
}

function setMenuButtonsColor(paramSelectedMenuName) {
    var vSizeSButton = $("#sizeS");
    var vSizeMButton = $("#sizeM");
    var vSizeLButton = $("#sizeL");

    switch (paramSelectedMenuName) {
        case "S":
            vSizeSButton.attr("class", "btn btn-warning w-75");
            vSizeMButton.attr("class", "btn btn-success w-75")
            vSizeLButton.attr("class", "btn btn-success w-75")
            break;
        case "M":
            vSizeSButton.attr("class", "btn btn-success w-75");
            vSizeMButton.attr("class", "btn btn-warning w-75")
            vSizeLButton.attr("class", "btn btn-success w-75")
            break;
        case "L":
            vSizeSButton.attr("class", "btn btn-success w-75");
            vSizeMButton.attr("class", "btn btn-success w-75")
            vSizeLButton.attr("class", "btn btn-warning w-75")
            break;
    }
}

function onBtnSizeSCLick() {
    "use strict";
    // map lựa chọn vào gSelectedMenuStructure
    gSelectedMenuStructure.menuName = "S";
    gSelectedMenuStructure.duongKinhCM = "20";
    gSelectedMenuStructure.suongNuong = 2;
    gSelectedMenuStructure.saladGr = 200;
    gSelectedMenuStructure.drink = 2;
    gSelectedMenuStructure.priceVND = 150000;


    //set trạng thái của nut - set the buttons style
    setMenuButtonsColor(gSelectedMenuStructure.menuName);

    console.log(gSelectedMenuStructure);
}

function onBtnSizeMCLick() {
    "use strict";
    // map lựa chọn vào gSelectedMenuStructure
    gSelectedMenuStructure.menuName = "M";
    gSelectedMenuStructure.duongKinhCM = "25";
    gSelectedMenuStructure.suongNuong = 4;
    gSelectedMenuStructure.saladGr = 300;
    gSelectedMenuStructure.drink = 3;
    gSelectedMenuStructure.priceVND = 200000;


    //set trạng thái của nut - set the buttons style
    setMenuButtonsColor(gSelectedMenuStructure.menuName);

    console.log(gSelectedMenuStructure);
}

function onBtnSizeLCLick() {
    "use strict";
    // map lựa chọn vào gSelectedMenuStructure
    gSelectedMenuStructure.menuName = "L";
    gSelectedMenuStructure.duongKinhCM = "30";
    gSelectedMenuStructure.suongNuong = 8;
    gSelectedMenuStructure.saladGr = 500;
    gSelectedMenuStructure.drink = 4;
    gSelectedMenuStructure.priceVND = 250000;


    //set trạng thái của nut - set the buttons style
    setMenuButtonsColor(gSelectedMenuStructure.menuName);

    console.log(gSelectedMenuStructure);
}

function onBtnHawaiiClick() {
    gSelectedPizzaType = "Hawaii";
    console.log(gSelectedPizzaType);
    setColorTypeButton(gSelectedPizzaType);
}

function onBtnHaiSanClick() {
    gSelectedPizzaType = "Hải sản";
    console.log(gSelectedPizzaType);
    setColorTypeButton(gSelectedPizzaType);
}

function onBtnThitHunKhoiClick() {
    gSelectedPizzaType = "Thịt hun khói";
    console.log(gSelectedPizzaType);
    setColorTypeButton(gSelectedPizzaType);
}

function setColorTypeButton(paramTypeButton) {
    var vHawaiiTypeButton = $("#hawaii");
    var vHaiSanTypeButton = $("#haisan");
    var vThitHunKhoiTypeButton = $("#thithunkhoi");

    switch (paramTypeButton) {
        case "Hawaii":
            vHawaiiTypeButton.attr("class", "btn btn-warning w-100");
            vHaiSanTypeButton.attr("class", "btn btn-success w-100");
            vThitHunKhoiTypeButton.attr("class", "btn btn-success w-100");
            break;
        case "Hải sản":
            vHawaiiTypeButton.attr("class", "btn btn-success w-100");
            vHaiSanTypeButton.attr("class", "btn btn-warning w-100");
            vThitHunKhoiTypeButton.attr("class", "btn btn-success w-100");
            break;
        case "Thịt hun khói":
            vHawaiiTypeButton.attr("class", "btn btn-success w-100");
            vHaiSanTypeButton.attr("class", "btn btn-success w-100");
            vThitHunKhoiTypeButton.attr("class", "btn btn-warning w-100");
            break;
    }
}

function getDrink() {
    $.ajax({
        url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
        type: "GET",
        dataType: "json",
        success: function (drinkObj) {

            console.log(drinkObj);
            addDrinkToSelect(drinkObj);
        },
        error: function () {
            console.log("fail");
        }
    })
}

function addDrinkToSelect(paramDrinkObj) {
    var vSelectDrink = $("#select-drink");

    for (var Drink of paramDrinkObj) {
        $("<option>", { value: Drink.maNuocUong, text: Drink.tenNuocUong }).appendTo(vSelectDrink);
    }
}


